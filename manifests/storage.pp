define tahoe::storage (
  $directory,
  $introducer_furl,
  $ensure = present,
  $webport = '',
  $stats_gatherer_furl = false,
  $helper = false,
  $helper_furl = ''
) {

  tahoe::client { $name:
    ensure               => $ensure,
    directory            => $directory,
    introducer_furl      => $introducer_furl,
    webport              => $webport,
    stats_gatherer_furl  => $stats_gatherer_furl,
    helper               => $helper,
    helper_furl          => $helper_furl,
    storage              => true,
  }
}
